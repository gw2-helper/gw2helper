package es.uniovi.informaticamovil.gw;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Item implements Parcelable {

    private int id;
	private String name;

    private String icon;
    private String type;
    private String description;
    private String rarity;
    private int level;
    private int vendor_value;
    private ArrayList<String> flags;
    private ArrayList<String> game_types;
    private ArrayList<String> restrictions;


    public Item(String name, String description, int level, String rarity, String icon, String type, int vendor_value, ArrayList<String> flags, ArrayList<String> game_types, ArrayList<String> restrictions){
        this.name = name;

        this.icon = icon;
        this.type = type;
        this.description = description;
        this.rarity = rarity;
        this.level = level;
        this.vendor_value = vendor_value;
        this.flags = flags;
        this.game_types=game_types;
        this.restrictions = restrictions;
    }
    public Item(String name, String description, int level, String rarity, String icon, String type, int vendor_value){
        this.name = name;

        this.icon = icon;
        this.type = type;
        this.description = description;
        this.rarity = rarity;
        this.level = level;
        this.vendor_value = vendor_value;
    }

    public Item(){}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String mDescription) {
        this.description = mDescription;
    }

	public String getName() {
		
		return name;
	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRarity() {
        return rarity;
    }

    public void setRarity(String rarity) {
        this.rarity = rarity;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getVendor_value() {
        return vendor_value;
    }

    public void setVendor_value(int vendor_value) {
        this.vendor_value = vendor_value;
    }

    public ArrayList<String> getFlags() {
        return flags;
    }

    public void setFlags(ArrayList<String> flags) {
        this.flags = flags;
    }

    public ArrayList<String> getGame_types() {
        return game_types;
    }

    public void setGame_types(ArrayList<String> game_types) {
        this.game_types = game_types;
    }

    public ArrayList<String> getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(ArrayList<String> restrictions) {
        this.restrictions = restrictions;
    }


    public Item(Parcel parcel) {
        readFromParcel(parcel);
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(icon);
        dest.writeString(type);
        dest.writeString(description);
        dest.writeString(rarity);
        dest.writeInt(level);
        dest.writeInt(vendor_value);
        dest.writeList(this.flags);
        dest.writeList(game_types);
        dest.writeList(restrictions);

    }
    private void readFromParcel(Parcel parcel) {
        id = parcel.readInt();
        name =parcel.readString();
        icon =parcel.readString();
        type =parcel.readString();
        description =parcel.readString();
        rarity = parcel.readString();
        level = parcel.readInt();
        vendor_value = parcel.readInt();
        flags = parcel.readArrayList(String.class.getClassLoader());
        game_types = parcel.readArrayList(String.class.getClassLoader());
        restrictions = parcel.readArrayList(String.class.getClassLoader());


    }
    public static final Creator<Item> CREATOR =
            new Creator<Item>() {

                public Item createFromParcel(Parcel in) {
                    return new Item(in);
                }

                public Item[] newArray(int size) {
                    return new Item[size];
                }
            };
}
