package es.uniovi.informaticamovil.gw;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.NumberPicker;

public class MyDialogFragment extends DialogFragment {

    public interface MyDialogFragmentListener {
        void onFinishEditDialog();
    }
    SharedPreferences prefs;

    private MyDialogFragment mdf;
    // if button is clicked, close the custom dialog
    private static final String PREFERENCES =  "preferences";
    private NumberPicker horas;
    private NumberPicker minutos;

    public static MyDialogFragment newInstance(){
        MyDialogFragment f;
        f = new MyDialogFragment();
        return f;
    }
    public MyDialogFragment() {
        // Empty constructor required for DialogFragment
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs =getActivity().getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);

        setRetainInstance(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialogo, container);
        Button aceptar = (Button) view.findViewById(R.id.aceptar);
        horas = (NumberPicker) view.findViewById(R.id.hora);
        minutos = (NumberPicker) view.findViewById(R.id.minuto);
        mdf = this;
        //El máximo valor será 1 hora y 59 minutos
        horas.setMaxValue(1);
        horas.setMinValue(0);
        minutos.setMaxValue(59);
        minutos.setMinValue(0);

        if (savedInstanceState == null) {
            horas.setValue(prefs.getInt("hora", 0));
            minutos.setValue(prefs.getInt("minuto", 0));
        }
        else{
            horas.setValue(savedInstanceState.getInt("hour", 0));
            minutos.setValue(savedInstanceState.getInt("minute", 0));
        }
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor prefsEditor = prefs.edit();
                prefsEditor.putInt("hora", horas.getValue());
                prefsEditor.putInt("minuto", minutos.getValue());

                prefsEditor.apply();
                MyDialogFragmentListener activity = (MyDialogFragmentListener) getActivity();
                activity.onFinishEditDialog();

                mdf.dismiss();
            }
        });
        Button cancelar = (Button) view.findViewById(R.id.cancelar);

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdf.dismiss();
            }
        });
        return view;
    }
    @Override
    public void onDestroyView()
    {
        Dialog dialog = getDialog();

        if ((dialog != null) && getRetainInstance())
            dialog.setDismissMessage(null);

        super.onDestroyView();
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Para guardar el valor cuando se rota
        outState.putInt("hour", horas.getValue());
        outState.putInt("minute", minutos.getValue());
    }


}
