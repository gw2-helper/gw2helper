package es.uniovi.informaticamovil.gw;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by cyrak on 14/3/15.
 */
public class JSONManager {
    public static JSONObject getJSONfromURLObject(String url){
        InputStream is = null;
        String result = "";
        JSONObject json = null;
        try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(url);
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
        }catch(Exception e){}

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
                System.out.println("línea almacenada: "+line);
            }
            is.close();
            result=sb.toString();
        } catch(Exception e){}

        try{
            json = new JSONObject(result);
            if (json == null){
                System.out.println("JSON nulo");
            }
        }catch(JSONException e){}

        return json;
    }

    public static JSONArray getJSONfromURLArray(String url){
        InputStream is = null;
        String result = "";
        JSONArray json = null;
        try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(url);
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
        }catch(Exception e){}

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
                System.out.println("línea almacenada: "+line);
            }
            is.close();
            result=sb.toString();
        } catch(Exception e){}

        try{
            json = new JSONArray(result);
            if (json == null){
                System.out.println("JSON nulo");
            }
        }catch(JSONException e){}

        return json;
    }

    public static JSONObject getJSONfromURL(String url){
        InputStream is = null;
        String result = "";
        JSONObject json = null;
        try{
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(url);
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
        }catch(Exception e){}

        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
                System.out.println("línea almacenada: "+line);
            }
            is.close();
            result=sb.toString();
        } catch(Exception e){}

        try{
            json = new JSONObject(result);
            if (json == null){
                System.out.println("he entrado aquí");
            }
        }catch(JSONException e){}

        return json;
    }



}
