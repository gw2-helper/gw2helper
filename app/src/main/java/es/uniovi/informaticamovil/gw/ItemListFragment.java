package es.uniovi.informaticamovil.gw;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ItemListFragment extends android.support.v4.app.Fragment implements
        AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    public ItemAdapter mAdapter = null;

    View rootView;


    String tipo = "";
    int nivel = 0;
    Spinner spinner;
    Spinner spinner2;

    DownloadDataTask downloadTask;

    public final static String nombreBundle = "bundle";
    boolean buscando = false;
    ImageButton botonBusca;







    TextView nombreABuscar = null;
    Callbacks mCallback;
    ArrayList<Item> arrayDescargados;

    public static ItemListFragment newInstance() {
        ItemListFragment fragment = new ItemListFragment();
        return fragment;
    }





    public interface Callbacks {
        public void onItemSelected(Item item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        //Al reemplazar fragmentos, el de detrás cuando sale de la pila no se recrea



            rootView = inflater.inflate(R.layout.course_list_fragment,
                    container, false);




        ListView lvItems = (ListView) rootView.findViewById(R.id.list_view_courses);

        mAdapter = new ItemAdapter(getActivity(), createItemList());
        lvItems.setOnItemClickListener(this);
        lvItems.setAdapter(mAdapter);
        nombreABuscar = (TextView) rootView.findViewById(R.id.editText);
        botonBusca = (ImageButton) rootView.findViewById(R.id.button);

        spinner = (Spinner) rootView.findViewById(R.id.spinner);
        spinner2 = (Spinner) rootView.findViewById(R.id.spinner2);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.Tipos, android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(),
                R.array.Niveles, android.R.layout.simple_spinner_dropdown_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        spinner2.setAdapter(adapter2);
        spinner2.setOnItemSelectedListener(this);

        if(savedInstanceState == null) {
            arrayDescargados = new ArrayList<Item>();
            nombreABuscar = (TextView) rootView.findViewById(R.id.editText);

        }
        else{

            tipo = savedInstanceState.getString("tipo");
            nivel = savedInstanceState.getInt("nivel");
            buscando = savedInstanceState.getBoolean("buscando");
            if(buscando){

                String cadena = savedInstanceState.getString("cadena");
                downloadTask = new DownloadDataTask(arrayDescargados,cadena, tipo, nivel);
                downloadTask.execute();
            }
            else {
                arrayDescargados = savedInstanceState.getParcelableArrayList(nombreBundle);
                for (int i = 0; i < arrayDescargados.size(); i++) {

                    if (tipo != "") {
                        if (arrayDescargados.get(i).getType().equals(tipo)) {
                            if (arrayDescargados.get(i).getLevel() <= nivel) {
                                mAdapter.addItem(arrayDescargados.get(i));
                                mAdapter.notifyDataSetChanged();

                            }

                        }
                    } else {
                        if (arrayDescargados.get(i).getLevel() <= nivel) {
                            mAdapter.addItem(arrayDescargados.get(i));
                            mAdapter.notifyDataSetChanged();
                        }
                    }

                }
            }

        }

        botonBusca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context contexto = getActivity().getApplicationContext();

                if(checkConn(contexto)==false){
                    Toast.makeText(contexto, R.string.no_conn,
                            Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(contexto, R.string.default_searching,
                            Toast.LENGTH_LONG).show();

                    String cadena = nombreABuscar.getText().toString();
                    String tip = "";
                    //Reiniciar el listview con cada nueva busqueda
                    mAdapter.clear();
                    mAdapter.notifyDataSetChanged();
                    if (tipo.equals("")) {
                        tip = getResources().getString(R.string.all);
                    }


                    Toast.makeText(contexto, getResources().getString(R.string.searching) + cadena + ", " + getResources().getString(R.string.type) + tip + ", " + getResources().getString(R.string.level) + nivel,
                            Toast.LENGTH_LONG).show();
                    lanzaDownloadDataTask();
                }
            }
        });


        return rootView;




    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(nombreBundle, arrayDescargados);
        outState.putString("tipo",tipo);

        outState.putInt("nivel", nivel);

        if(downloadTask != null && downloadTask.getStatus() == DownloadDataTask.Status.RUNNING){
            outState.putBoolean("buscando", true);
            outState.putString("cadena", nombreABuscar.getText().toString());
            downloadTask.cancel(true);
        }
        else
            outState.putBoolean("buscando", false);
    }


    //Control de los filtros
    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using

        Spinner spinner = (Spinner) parent;
        if(spinner.getId() == R.id.spinner)
        {
            tipo = (String) parent.getItemAtPosition(pos);
            if(tipo.equals("Todos") || tipo.equals("All")){
                tipo = "";

            }
        }
        else if(spinner.getId() == R.id.spinner2)
        {
            nivel = Integer.parseInt((String) parent.getItemAtPosition(pos));
        }


    }




    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() +
                    " must implement Callbacks");
        }
    }

    private List<Item> createItemList() {
        ArrayList<Item> items = new ArrayList<Item>();

        return items;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        Item item = (Item) parent.getItemAtPosition(position);
        mCallback.onItemSelected(item);


    }

    public void lanzaDownloadDataTask(){
        downloadTask = new DownloadDataTask(arrayDescargados,nombreABuscar.getText().toString(), tipo, nivel);
        downloadTask.execute();
    }

    public class DownloadDataTask extends AsyncTask<String, Void, ArrayList<Item>> {
        ArrayList<Item> array;
        String texto;
        String tipo;
        int nivel;

        public DownloadDataTask(ArrayList<Item> array, String texto, String tipo, int nivel) {
            this.array = array;
            this.texto = texto;
            this.tipo = tipo;
            this.nivel = nivel;

        }

        protected ArrayList doInBackground(String... urls) {
            ArrayList<Item> items=new ArrayList<Item>();
            JSONParserItems jparser = new JSONParserItems(getActivity().getApplicationContext(), texto);
            try {
                items = jparser.readJSONItems();
                return items;
            } catch (Exception e) {
                //e.printStackTrace();

            }

            return items;
        }

        protected void onPostExecute(ArrayList<Item> result) {
            arrayDescargados = result;

            for(int i = 0; i <arrayDescargados.size(); i++){

                if(tipo != "") {
                    if (arrayDescargados.get(i).getType().equals(tipo)) {
                        if(arrayDescargados.get(i).getLevel() <= nivel) {

                            mAdapter.addItem(arrayDescargados.get(i));
                            mAdapter.notifyDataSetChanged();
                        }
                    }
                }
                else{
                    if(arrayDescargados.get(i).getLevel() <= nivel) {

                        mAdapter.addItem(arrayDescargados.get(i));
                        mAdapter.notifyDataSetChanged();
                    }
                }

            }



        }
    }

    public static boolean checkConn(Context ctx){
        ConnectivityManager conMgr = ( ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if(i == null)
            return false;
        if(!i.isConnected())
            return  false;
        if(!i.isAvailable())
            return false;
        return true;
    }



}









