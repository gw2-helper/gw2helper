package es.uniovi.informaticamovil.gw;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


public class ItemDetailsActivity extends ActionBarActivity {

    TextView mTexto;
    ImageView mImageView;

    //public static final String DESCRIPTION =
       //     "es.uniovi.imovil.user.courses.DESCRIPTION";

    public static final Item item =    new Item();

    @Override
    protected void onCreate(Bundle savedInstanceState) {



        Intent intent = getIntent();
        //String description = intent.getStringExtra(DESCRIPTION);
        Item it = intent.getParcelableExtra("item");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.course_details_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // Existe el contenedor del fragmento?
        if (findViewById(R.id.fragment_container) != null) {
            // Si estamos restaurando desde un estado previo no hacemos nada
            if (savedInstanceState != null) {
                return;
            }
            // Crear el fragmento pasándole el parámetro
            //ItemDetailsFragment fragment =
             //       ItemDetailsFragment.newInstance(it.getIcon());
            ItemDetailsFragment fragment =
                           ItemDetailsFragment.newInstance(it);
            // Añadir el fragmento al contenedor 'fragment_container'
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, fragment).commit();
        }









    }

    public boolean onOptionsItemSelected(MenuItem item){
        onBackPressed();
        return true;

    }















}
