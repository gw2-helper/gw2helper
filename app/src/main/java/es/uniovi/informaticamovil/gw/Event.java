package es.uniovi.informaticamovil.gw;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Calendar;
import java.util.TimeZone;

public class Event implements Parcelable, Serializable {

    private String mName;
    private String mTeacher;
    private int Rep;
    private int nivel;
    private String zona;
    private boolean especial;
    private int id;
    private Calendar c;
    private Calendar primera;
    private boolean favorito;
    private String mDescription;
    private String nombre_imagen;
    public Calendar getC() {
        return c;
    }

    public void setC(Calendar c) {
        this.c = c;
    }

    public boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }

    public int getRep() {
        return Rep;
    }

    public String getNombre_imagen() {
        return nombre_imagen;
    }

    public boolean isEspecial() {
        return especial;
    }

    public Calendar getPrimera() {
        return primera;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getZona() {
        return zona;
    }

    public String getName() {

        return mName;
    }
    public void setNivel(int nivel){
        this.nivel=nivel;
    }

    public int getNivel() {

        return nivel;
    }
    //Constructor para los eventos especiales
    public Event(String nombre, Calendar calendar, boolean b, int nivel, int id, String s, String imagen) {
        mName = nombre;
        primera = calendar;
        c = calendar;
        especial=b;
        this.nivel=nivel;
        this.id = id;
        zona=s;
        nombre_imagen=imagen;
        c.setTimeZone(TimeZone.getTimeZone("CET"));

    }

    //Constructor para los eventos no especiales
    public Event(String name, Calendar cal, int i, int level, String imagene, int id, String zona, Calendar primera) {

        if (name == null ||  name.isEmpty() ) {
            throw new IllegalArgumentException();
        }
        Rep = i;
        mName = name;

        favorito=false;
        c = cal;
        nivel = level;
        nombre_imagen=imagene;
        c.setTimeZone(TimeZone.getTimeZone("UTC"));
        especial = false;
        this.id = id;
        this.zona=zona;
        this.primera = primera;

    }


    public Event(Parcel parcel) {
        super();
        readFromParcel(parcel);
    }
    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mTeacher);
        dest.writeString(mDescription);
        dest.writeString(nombre_imagen);
    }
    private void readFromParcel(Parcel parcel) {
        mName = parcel.readString();
        mTeacher = parcel.readString();
        mDescription = parcel.readString();
        nombre_imagen = parcel.readString();
    }
    public static final
            Creator<Event> CREATOR =
            new Creator<Event>() {
                public Event createFromParcel(Parcel in) {
                    return new Event(in);
                }
                public Event[] newArray(int size) {
                    return new Event[size];
                }
            };


}
