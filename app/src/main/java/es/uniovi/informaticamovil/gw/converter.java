package es.uniovi.informaticamovil.gw;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link converter.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link converter#newInstance} factory method to
 * create an instance of this fragment.
 */
public class converter extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    static JSONObject jObj = null;

    int mGemtoCoin;

    EditText mEditTextCoins;
    EditText mEditTextGems;
    Button convertoCoins;
    Button convertoGems;
    String urlgems = "https://api.guildwars2.com/v2/commerce/exchange/gems?quantity=";
    String urlcoins = "https://api.guildwars2.com/v2/commerce/exchange/coins?quantity=";
    String url;
    String coinsPerGem;
    String cantidad_gemas;
    String noconexion;
    String cantidad_monedas;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment converter.
     */
    // TODO: Rename and change types and number of parameters
    public static converter newInstance(String param1, String param2) {
        converter fragment = new converter();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public converter() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView;
        rootView = inflater.inflate(R.layout.converter_fragmento,
                container, false);


        // Inflate the layout for this fragment


        mEditTextCoins = (EditText) rootView.findViewById(R.id.editTextMoneda);
        mEditTextGems = (EditText) rootView.findViewById(R.id.editTextGema);
        convertoCoins =  (Button) rootView.findViewById(R.id.buttontoCoins);
        convertoGems = (Button) rootView.findViewById(R.id.buttontoGems);
        cantidad_gemas = rootView.getResources().getString(R.string.cantidad_gemas);
        cantidad_monedas = rootView.getResources().getString(R.string.cantidad_monedas);
        noconexion = rootView.getResources().getString(R.string.fallo_conexion);

       /* if(savedInstanceState!=null){
            String gemas = savedInstanceState.getString("gemas");
            String monedas = savedInstanceState.getString("monedas");
            mEditTextCoins.setText(monedas);
            mEditTextGems.setText(gemas);
        }*/


        convertoCoins.setOnClickListener(new View.OnClickListener() {
            @Override
                public void onClick(View v) {
                String valorGemas = "";
                valorGemas = mEditTextGems.getText().toString();
                valorGemas = valorGemas.trim();
                Context contexto = getActivity().getApplicationContext();

            if(valorGemas.length() == 0){

                    Toast.makeText(contexto, cantidad_gemas, Toast.LENGTH_LONG).show();

             }

            else if (valorGemas.length() != 0){
                    if (valorGemas.equals("1") ){
                        Toast.makeText(contexto, cantidad_gemas, Toast.LENGTH_LONG).show();
                    }
                    else if(valorGemas.equals("0")){
                        Toast.makeText(contexto, cantidad_gemas, Toast.LENGTH_LONG).show();
                    }
                    else{
                        if(checkConn(contexto)==false){
                            Toast.makeText(contexto, noconexion, Toast.LENGTH_LONG).show();
                        }
                        else {
                            url = urlgems.concat(valorGemas);
                            new UpdateRateTask().execute(url);
                        }

                    }
                }

            }
        });

        convertoGems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context contexto2 = getActivity().getApplicationContext();
                if (checkConn(contexto2) == false) {
                    Toast.makeText(contexto2, noconexion, Toast.LENGTH_LONG).show();
                } else {
                    String valorCoins = mEditTextCoins.getText().toString();
                    valorCoins = valorCoins.trim();
                    if (valorCoins != "") {
                        url = urlcoins.concat(valorCoins);
                        new UpdateRateTasktoGemas().execute(url);

                    }
                    else {
                        Toast.makeText(contexto2, cantidad_monedas, Toast.LENGTH_LONG).show();
                    }


                }
            }
        });

        return rootView;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public static boolean checkConn(Context ctx){

        ConnectivityManager conMgr = (ConnectivityManager)ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        if (!i.isAvailable())
            return false;
        return true;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void convert(EditText editTextSource, EditText editTextDestination,
                        int ConversionFactor) {

        /*String StringSource = editTextSource.getText().toString();

        double NumberSource;
        try {
            NumberSource = Double.parseDouble(StringSource);
        } catch (NumberFormatException nfe) {
            return;
        }
        double NumberDestination = NumberSource * ConversionFactor;*/
        if(ConversionFactor ==0){
            Context contexto = getActivity().getApplicationContext();
            Toast.makeText(contexto, cantidad_monedas,Toast.LENGTH_LONG).show();
        }

        String StringDestination = Integer.toString(ConversionFactor);

        editTextDestination.setText(StringDestination);
    }

    public class UpdateRateTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String result) {
            //Asignar a la variable del ratio
            if(result!=null){
                mGemtoCoin=Integer.parseInt(result);
                convert(mEditTextGems,mEditTextCoins,mGemtoCoin);
            }
            else{
                //Para el caso en el que la conexion falle poder utilizar la aplicacion
                //con un valor por defecto
                mGemtoCoin=1;
            }

        }

        @Override
        protected String doInBackground(String... urls) {
            // urls es un array de elementos. El primero ser� urls[0]
            try {
                System.out.println("\ndoInBackground "+urls[0]);
                return getCurrencyRateUsdRate(urls[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private String getCurrencyRateUsdRate(String url) throws IOException {
            InputStream stream=openUrl(url);
            String datos=readStream(stream);
            System.out.println("\ngetCurrencyRateUsdRate "+datos);
            return parseDataFromNetwork(datos);
        }

        protected InputStream openUrl(String urlString) throws IOException {
            InputStream is = null;


            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(urlString);
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
            }catch(Exception e){}

            return is;


        }

        protected String readStream(InputStream urlStream) throws IOException {
            BufferedReader r = new BufferedReader(new InputStreamReader(urlStream,"iso-8859-1"),8);
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
            urlStream.close();

            Log.v("total", "total" + total);
            return total.toString();
        }

        private String parseDataFromNetwork(String data) {
            String error = "";
            try {
                jObj = new JSONObject(data);
                if (jObj != null){

                    coinsPerGem = jObj.getString("quantity");


                }
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
                coinsPerGem = 0+"";
            }





            return coinsPerGem;

        }

    }

    public class UpdateRateTasktoGemas extends AsyncTask<String, Void, String> {

        @Override
        protected void onPostExecute(String result) {
            //Asignar a la variable del ratio
            if(result!=null){
                mGemtoCoin=Integer.parseInt(result);
                convert(mEditTextCoins,mEditTextGems,mGemtoCoin);
            }
            else{
                //Para el caso en el que la conexion falle poder utilizar la aplicacion
                //con un valor por defecto
                mGemtoCoin=1;
            }

        }

        @Override
        protected String doInBackground(String... urls) {
            // urls es un array de elementos. El primero ser� urls[0]
            try {
                System.out.println("\ndoInBackground "+urls[0]);
                return getCurrencyRateUsdRate(urls[0]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        private String getCurrencyRateUsdRate(String url) throws IOException {
            InputStream stream=openUrl(url);
            String datos=readStream(stream);
            System.out.println("\ngetCurrencyRateUsdRate "+datos);
            return parseDataFromNetwork(datos);
        }

        protected InputStream openUrl(String urlString) throws IOException {
            InputStream is = null;


            try{
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(urlString);
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
            }catch(Exception e){}

            return is;


        }

        protected String readStream(InputStream urlStream) throws IOException {
            BufferedReader r = new BufferedReader(new InputStreamReader(urlStream,"iso-8859-1"),8);
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
            urlStream.close();

            Log.v("total", "total" + total);
            return total.toString();
        }

        private String parseDataFromNetwork(String data) {
            String error = "";
            try {
                jObj = new JSONObject(data);
                if (jObj != null){
                    /*error = jObj.getString("text");*/

                    coinsPerGem = jObj.getString("quantity");


                }
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
                coinsPerGem = 0+"";
            }


            return coinsPerGem;

        }

    }




    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
    /*
    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        String valorGemas = mEditTextGems.getText().toString();
        String valorCoins = mEditTextCoins.getText().toString();
        outState.putString("gemas",valorGemas );
        outState.putString("monedas", valorCoins);
    }*/



}
