package es.uniovi.informaticamovil.gw;

import java.io.Serializable;
import java.util.ArrayList;

/*
    Clase para poder pasar un arraylist al servicio
 */
public class DataWrapper implements Serializable {

    private ArrayList<Event> parliaments;

    public DataWrapper(ArrayList<Event> data) {
        this.parliaments = data;
    }

    public ArrayList<Event> getParliaments() {
        return this.parliaments;
    }

}